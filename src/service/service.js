const termDate = (out) => { 
  const timeDiff = new Date (out) - new Date ()
   
  const days = Math.ceil(timeDiff / (1000 * 3600 * 24))

  return days >= 0
}

const getTimer = (out) => {
  let startDate = +new Date(out)
    
  let differenceTime =  new Date( startDate - +new Date())

  const timeDiff = new Date (out) - new Date ()
  const days = Math.ceil(timeDiff / (1000 * 3600 * 24))
    

  const itemDifferenceTime = {
    itemSecond: differenceTime.getSeconds(),
    itemHour: differenceTime.getHours(),
    itemMinute: differenceTime.getMinutes()
  }

  const titles = {
    second: ['секунда', 'секунды', 'секунд'],
    minute: ['минута', 'минуты', 'минут'],
    hour: ['час', 'часа', 'часов'],
    day: ['день', 'дня', 'дней']
  }

  const refSecond = declension(titles.second, itemDifferenceTime.itemSecond)
  const refMinute = declension(titles.minute, itemDifferenceTime.itemMinute)
  const refHour = declension(titles.hour, itemDifferenceTime.itemHour)
  const refDay = declension(titles.day, days)
  
  return [
    { 
      timer: days,
      days: refDay
    },
    { 
      timer: itemDifferenceTime.itemHour,
      hours: refHour
    },
    {
      timer: itemDifferenceTime.itemMinute,
      minutes: refMinute
    },
    {
      timer: itemDifferenceTime.itemSecond,
      seconds: refSecond
    }
  ]
}

const declension = (titles, val) => {
  const cases = [ 2, 0, 1, 1, 1, 2 ];
  return titles[(val % 100 > 4 && val % 100 < 20) ? 2 : cases[(val % 10 < 5) ? val % 10 : 5]];
}

export default {
  termDate,
  getTimer
}