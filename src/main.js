import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import serviceContainer from './service/serviceContainer'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


Vue.config.productionTip = false

import VueYandexMetrika from 'vue-yandex-metrika'  

Vue.use(VueYandexMetrika, {
  id: 87947197,
  env:'production'
})

new Vue({
  router,
  store,
  provide: serviceContainer,
  render: h => h(App)
}).$mount('#app')


  
